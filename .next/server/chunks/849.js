exports.id = 849;
exports.ids = [849];
exports.modules = {

/***/ 974:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Section)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);



function Section({
  children,
  element_id,
  background,
  icon,
  height
}) {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: element_id,
    children: [children, icon && icon.length > 0 && icon.map((shape, id) => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
        className: `icon-shape-${id}  ${shape.vertical ? shape.vertical : ''} ${shape.horizontal ? shape.horizontal : ''}`,
        src: `/static/images/${shape.image}`
      }, id), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("style", {
        children: `
              .${element_id} .icon-shape-${id} {
                position: absolute;
                z-index: 0;
              }

              .${element_id} .icon-shape-${id}.top {
                top: 0;
              }

              .${element_id} .icon-shape-${id}.bottom {
                bottom: 0;
              }

              .${element_id} .icon-shape-${id}.right {
                right: 0;
              }

              .${element_id} .icon-shape-${id}.left {
                left: 0;
              }
          `
      })]
    })), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("style", {
      children: `
          .${element_id} {
            width: 100%;
            background: ${background};
            position: relative;
            height: ${height};
          }

        `
    })]
  });
}

/***/ }),

/***/ 34:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Layout)
});

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(701);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(297);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(282);
;// CONCATENATED MODULE: ./containers/head.js





class WebHead extends (external_react_default()).Component {
  render() {
    return /*#__PURE__*/(0,jsx_runtime_.jsxs)((head_default()), {
      children: [/*#__PURE__*/jsx_runtime_.jsx("title", {
        children: "PT. Era Mitra Perdana"
      }), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "icon",
        href: "/favicon.svg"
      }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
        name: "title",
        content: "PT. Era Mitra Perdana"
      }), /*#__PURE__*/jsx_runtime_.jsx("link", {
        href: "https://fonts.googleapis.com/icon?family=Material+Icons",
        rel: "stylesheet"
      }), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "stylesheet",
        type: "text/css",
        href: "/static/styles/css/slick.css"
      }), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "stylesheet",
        type: "text/css",
        href: "/static/styles/css/slick-theme.css"
      }), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "stylesheet",
        href: "/static/fonts/Bahnschrift.TTF",
        rel: "stylesheet"
      })]
    });
  }

}

/* harmony default export */ const head = (WebHead);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(664);
;// CONCATENATED MODULE: ./containers/header.js





class Header extends (external_react_default()).Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

  render() {
    const props = this.props;
    return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "navbar-header",
      children: [this.state.isToggleOn && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "sidebar mobile-only",
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "logo-wrapper",
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            href: "http://localhost:3000/",
            children: /*#__PURE__*/jsx_runtime_.jsx("img", {
              className: "logo",
              src: "/static/images/logo.png"
            })
          })
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "navbar-link",
          children: [/*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "http://localhost:3000/about-us",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: "About Us"
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "http://localhost:3000",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: "Products"
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "http://localhost:3000",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: "News & Info"
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "http://localhost:3000",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: "Career"
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "http://localhost:3000",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: "Contact Us"
            })
          })]
        })]
      }), this.state.isToggleOn && /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "sidebar-background mobile-only",
        onClick: this.handleClick
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "navbar-content-wrapper",
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "navbar-inner-content-wrapper",
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "logo-wrapper",
            children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
              className: "material-icons mobile-only",
              onClick: this.handleClick,
              children: "menu"
            }), /*#__PURE__*/jsx_runtime_.jsx("a", {
              href: "http://localhost:3000/",
              children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                className: "logo",
                src: "/static/images/logo.png"
              })
            })]
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "navbar-link desktop-only",
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: "navbar-link-wrapper",
              children: [/*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                href: "http://localhost:3000/about-us",
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  children: "About Us"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                href: "http://localhost:3000",
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  children: "Products"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                href: "http://localhost:3000",
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  children: "News & Info"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                href: "http://localhost:3000",
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  children: "Career"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                href: "http://localhost:3000",
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  children: "Contact Us"
                })
              })]
            })
          })]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("style", {
        children: `
                .desktop-only {
                    display: block;
                }

                .mobile-only {
                    display: none;
                }
                
                .navbar-header {
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 3;
                    width: 100vw;
                    padding: 0;
                }

                .navbar-content-wrapper {
                    width: 100%;
                    position: relative;
                    box-shadow: 0px 4px 10px rgba(145, 158, 171, 0.1);
                    background-color: #FFF;
                    padding: 0 20px;
                }

                .navbar-inner-content-wrapper {
                    display: flex;
                    align-items: center;
                    justify-content: space-between;
                    flex-wrap: wrap;
                    max-width: 1120px;
                    height: 76px;
                    margin: 0 auto;
                }

                .navbar-inner-content-wrapper .logo-wrapper {
                    display: flex;
                    align-items: center;
                }

                .navbar-inner-content-wrapper .logo {
                    width: 90%;
                }

                .navbar-link-wrapper {
                  display: flex;
                  align-items: center;
                }

                .navbar-link a {
                    font-family: 'Bahnschrift';
                    font-weight: 400;
                    font-size: 14px;
                    margin-left: 55px;
                }

                .navbar-link a.active {
                    font-family: 'Bahnschrift';
                    font-weight: 500;
                    margin-left: 55px;
                    text-decoration: none;
                    color: #4B8343;
                }

                @media only screen and (max-width: 800px){
                    .mobile-only {
                        display: block;
                    }
                    
                    .desktop-only {
                        display: none;
                    }

                    .navbar-inner-content-wrapper {
                        height: 56px;
                    }
                    
                    .navbar-inner-content-wrapper .logo, .sidebar .logo {
                        width: 150px;
                        margin: 0 16px;
                        padding-top: 2px;
                    }

                    .navbar-header {
                        position: absolute;
                    }

                    .sidebar {
                        width: 80%;
                        height: 100vh;
                        background-color: white;
                        position: fixed;
                        z-index: 100;
                        top: 0;
                        left: 0;
                    }

                    .sidebar-background {
                        width: 100%;
                        height: 100%;
                        z-index: 99;
                        background-color: rgba(0, 0, 0, 0.1);
                        position: fixed;
                        top: 0;
                        left: 0;
                    }

                    .sidebar .logo-wrapper {
                        display: flex;
                        align-items: center;
                        height: 56px;
                        justify-content: space-between;
                        padding: 16px 32px 16px 14px;
                        // border-bottom: 1px solid rgb(235,235,235,1);
                    }

                    .sidebar .navbar-link {
                        display: flex;
                        flex-direction: column;
                        margin-top: 45px;
                    }

                    .sidebar .navbar-link a {
                      font-family: Bahnschrift;
                      font-style: normal;
                      font-weight: normal;
                      font-size: 18px;
                      line-height: 22px;
                      text-transform: uppercase;
                        text-align: left;
                        margin: 16px 0 0 32px;
                    }

                }
                `
      })]
    });
  }

}

/* harmony default export */ const header = (Header);
;// CONCATENATED MODULE: ./components/button/BlackButton.js




class BlackButton extends (external_react_default()).Component {
  constructor(props) {
    super(props);
  }

  render() {
    const props = this.props;
    return props.link ? /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
      className: `btn-black ${props.element_id}`,
      href: props.link,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "black-button-wrapper",
        children: /*#__PURE__*/jsx_runtime_.jsx("p", {
          children: props.text
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("style", {
        children: `
                    .black-button-wrapper {
                        background: #626262;
                        display: flex;
                        flex-wrap: wrap;
                        justify-content: center;
                        align-content: center;
                        width: fit-content;
                        padding: 4px 18px;
                        border-radius: 10px;
                    }

                    .black-button-wrapper p {
                        color: white;
                        font-family: Bahnschrift;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 18px;
                        line-height: 22px;
                        margin: 0;
                    }

                    @media only screen and (max-width: 800px){
                        .black-button-wrapper p {
                          font-family: Bahnschrift;
                          font-style: normal !important;
                          font-weight: normal;
                          font-size: 10px;
                          line-height: 12px;
                        }

                        .black-button-wrapper {
                          padding: 5px 10px;
                          border-radius: 5px;
                        }

                        .btn-black {
                          display: flex;
                          justify-content: center;
                        }
                    }
                `
      })]
    }) : props.onClick && /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
      className: `btn-black ${props.element_id}`,
      onClick: props.onClick,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "black-button-wrapper",
        children: /*#__PURE__*/jsx_runtime_.jsx("p", {
          children: props.text
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("style", {
        children: `
                    .black-button-wrapper {
                        background: #626262;
                        display: flex;
                        flex-wrap: wrap;
                        justify-content: center;
                        align-content: center;
                        width: fit-content;
                        padding: 8px 22px;
                        border-radius: 10px;
                    }

                    .black-button-wrapper p {
                        color: white;
                        font-family: Bahnschrift;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 18px;
                        line-height: 22px;
                        margin: 0;
                    }

                    @media only screen and (max-width: 800px){
                        .black-button-wrapper p {
                            font-family: Bahnschrift;
                            font-style: normal !important;
                            font-weight: normal;
                            font-size: 10px;
                            line-height: 12px;
                        }

                        .black-button-wrapper {
                          padding: 5px 10px;
                          border-radius: 5px;
                        }

                        .btn-black {
                          display: flex;
                          justify-content: center;
                        }
                    }
                `
      })]
    });
  }

}

/* harmony default export */ const button_BlackButton = (BlackButton);
;// CONCATENATED MODULE: ./containers/footer.js






class footer_Header extends (external_react_default()).Component {
  constructor(props) {
    super(props);
  }

  render() {
    const props = this.props;
    return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "footer-container",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "footer upper",
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "footer-wrapper",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "footer-inner-content-wrapper",
            children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: "footer-left",
              children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
                children: "OUR LOCATION"
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: "location-group",
                children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
                  children: "Head Office - Jakarta"
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
                  children: ["Ruko Buaran Persada No. 31 ", /*#__PURE__*/jsx_runtime_.jsx("br", {}), "Jl. R. Soekamto Duren Sawit Jakarta 13450, Indonesia"]
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
                  className: "contact",
                  children: ["+62-21-86612458 (Fax: +62-21-86612365) ", /*#__PURE__*/jsx_runtime_.jsx("br", {}), " sales@eramitra.com"]
                })]
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: "location-group",
                children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
                  children: "Branch Office - Surabaya"
                }), /*#__PURE__*/jsx_runtime_.jsx("p", {
                  children: "Jl. Kebon Sari V No. 7G - Surabaya"
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
                  className: "contact",
                  children: ["+62-21-86612458 (Fax: +62-21-86612365) ", /*#__PURE__*/jsx_runtime_.jsx("br", {}), " sales@eramitra.com"]
                })]
              })]
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: "reach-us",
              children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
                children: "REACH US"
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "form-reach-us",
                children: /*#__PURE__*/jsx_runtime_.jsx("input", {
                  type: "text",
                  placeholder: "Name"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "form-reach-us",
                children: /*#__PURE__*/jsx_runtime_.jsx("input", {
                  type: "text",
                  placeholder: "Phone Number"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "form-reach-us",
                children: /*#__PURE__*/jsx_runtime_.jsx("input", {
                  type: "text",
                  placeholder: "Email"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "form-reach-us",
                children: /*#__PURE__*/jsx_runtime_.jsx("input", {
                  type: "text",
                  placeholder: "Company"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "form-reach-us",
                children: /*#__PURE__*/jsx_runtime_.jsx("textarea", {
                  rows: "5",
                  placeholder: "Message"
                })
              }), /*#__PURE__*/jsx_runtime_.jsx(button_BlackButton, {
                element_id: "btn-footer-submit",
                text: "Submit",
                onClick: () => {
                  alert("Submit");
                }
              })]
            })]
          })
        }), /*#__PURE__*/jsx_runtime_.jsx("img", {
          className: "footer-shape",
          src: "/static/images/footer-shape.svg"
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "footer bottom",
        children: /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "footer-wrapper",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "footer-inner-content-wrapper",
            children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "footer-left",
              children: /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "info-wrapper",
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  href: "http://localhost:3000",
                  children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                    className: "logo",
                    src: "/static/images/logo-white.png"
                  })
                })
              })
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: "hyperlink-column",
              children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: "col footer-link",
                children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
                  children: "Newsletter"
                }), /*#__PURE__*/jsx_runtime_.jsx("h6", {
                  className: "footer-description",
                  children: /*#__PURE__*/jsx_runtime_.jsx("i", {
                    children: "Never miss out our newest information"
                  })
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                  className: "form-subscribe",
                  children: [/*#__PURE__*/jsx_runtime_.jsx("input", {
                    type: "text",
                    placeholder: "Your Email"
                  }), /*#__PURE__*/jsx_runtime_.jsx(button_BlackButton, {
                    element_id: "btn-footer-subscribe",
                    text: "Subscribe",
                    onClick: () => {
                      alert("Subscribe");
                    }
                  })]
                }), /*#__PURE__*/jsx_runtime_.jsx("h2", {
                  children: "Follow Us"
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                  className: "social-media-wrapper",
                  children: [/*#__PURE__*/jsx_runtime_.jsx("a", {
                    href: "https://www.instagram.com/byebeli/",
                    children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                      className: "logo-social",
                      src: "/static/icons/instagram.svg"
                    })
                  }), /*#__PURE__*/jsx_runtime_.jsx("a", {
                    href: "http://line.me/R/ti/p/@ywh9023y",
                    children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                      className: "logo-social",
                      src: "/static/icons/line.svg"
                    })
                  })]
                })]
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: "col footer-link page",
                children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
                  children: "Page"
                }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                  href: "http://localhost:3000",
                  children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                    children: "About Us"
                  })
                }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                  href: "http://localhost:3000",
                  children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                    children: "Career"
                  })
                }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                  href: "http://localhost:3000",
                  children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                    children: "Contact Us"
                  })
                }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                  href: "http://localhost:3000",
                  children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                    children: "Customer Service"
                  })
                })]
              })]
            })]
          })
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("style", {
        children: `

                .col {
                    display: flex;
                    flex-direction: column;
                }

                .desktop-only {
                    display: block;
                }

                .mobile-only {
                    display: none;
                }
                
                .footer {
                    width: 100vw;
                    display: flex;
                    align-items: center;
                    // height: 555px;
                    // padding-bottom: 30px;
                }

                .footer.bottom {
                  background: #0B9A7E;
                  height: 373px;
                }
                
                .footer.upper {
                  background: #ECECEC;
                  height: 638px;
                  position: relative;
                }

                .footer-shape {
                  position: absolute;
                  bottom: 0;
                  right: 0;
                  z-index: 0;
                }

                .footer-wrapper {
                    padding: 0;
                    max-width: 1120px;
                    margin: 0 auto;  
                    width: 100%;
                }

                .footer-wrapper.bottom {
                  background: #0B9A7E;
                }

                .footer-inner-content-wrapper {
                    display: flex;
                    justify-content: space-between;
                }

                .footer-left {
                    display: flex;
                    flex-wrap: wrap;
                    width: 45%;
                }
                .reach-us {
                  display: inline-block;
                  min-width: 587px;
                }

                .reach-us h2 {
                  margin-bottom: 24px;
                }
                
                .location-group .contact {
                  font-style: normal;
                }

                .form-reach-us {
                  margin-bottom: 16px;
                }

                .form-reach-us input, .form-reach-us textarea {
                  font-family: Calibri;
                  width: 100%;    
                  border: 1px solid #0E0E0E;
                  background: transparent;
                  padding: 6px 10px;
                  font-size: 16px;
                  line-height: 20px;
                  color: #757575;
                }

                .logo {
                    width: 100%;
                }
                
                .hyperlink-column {
                    display: grid;
                    grid-template-columns: 2fr 1.5fr;
                    grid-gap: 30px 50px;
                    min-width: 60%;
                }

                .info-wrapper {
                    padding-right: 55px;
                    display: flex;
                    flex-direction: column;
                    justify-content: center;
                }

                .footer-link.page {
                  padding-right: 20px;
                }

                .footer.upper h2 {
                  font-family: 'Bahnschrift';
                  font-style: normal;
                  font-weight: 600;
                  font-size: 24px;
                }

                .footer-link h2, .contact-us h2, .follow-us h2 {
                    font-family: 'Bahnschrift';
                    font-style: normal;
                    font-weight: 500;
                    font-size: 18px;
                    line-height: 30px;
                    color: #FFFFFF;
                    margin: 0 0 10px 0;
                }

                .footer-link a, .footer-link p {
                    font-family: 'Bahnschrift';
                    font-style: normal;
                    font-weight: normal;
                    font-size: 16px;
                    line-height: 24px;  
                    color: #FFFFFF;
                    margin: 0;                
                }

                .footer-link.page a {
                  padding-top: 18px;   
                }

                .location-group h2 {
                  margin-bottom: 15px;
                }

                .location-group p {
                  font-family: Calibri;
                  font-style: italic;
                  font-weight: normal;
                  font-size: 16px;
                  line-height: 20px;
                }
                
                .contact-us p {
                    color: white;
                    font-size: 13px;
                    font-weight: 500;
                    font-family: Open Sans;
                    margin: 0;
                }

                .social-media-wrapper {
                    display: flex;
                }

                .social-media-wrapper .logo-social {
                    margin-right: 5px;
                }
                
                .copyright {
                    color: #4B8343;
                    display: flex;
                    justify-content: center;
                    padding-top: 10px;
                }

                .copyright span {
                    font-size: 18px;
                    line-height: 22px;
                    margin-right: 4px;
                }

                .copyright p {
                    color: #4B8343;
                    margin: 0;
                    letter-spacing: 0em;
                    text-align: left;   
                    font-family: Poppins;
                    font-style: normal;
                    font-weight: normal;
                    font-size: 18px;
                    line-height: 27px;
                }
                
                .footer {
                  padding: 0 20px;
                }

                .footer-description {
                  font-family: Calibri;
                  font-style: italic;
                  font-weight: normal;
                  font-size: 12px;
                  line-height: 15px;
                  color: #FFFFFF;
                  margin: -7px 0 15px 0;
                }

                .form-subscribe input {
                  font-family: Calibri;
                  width: 100%;    
                  border: 1px solid #FFFFFF;
                  background: #FFFFFF;
                  padding: 6px 10px;
                  font-size: 16px;
                  line-height: 20px;
                  color: #757575;
                  border-radius: 10px 0 0 10px;
                }

                .form-subscribe {
                  display: flex;
                  margin-bottom: 20px;
                }

                .form-subscribe .black-button-wrapper {
                  border-radius: 0 10px 10px 0 !important;
                }

                @media only screen and (max-width: 800px){
                    .mobile-only {
                        display: block;
                    }
                    
                    .desktop-only {
                        display: none;
                    }

                    .footer {
                        width: 100vw;
                        height: 416px;
                        background-color: #ffffff;
                        background-image: url('static/images/footer.png');
                        background-size: 100% 100%;
                        background-position: top center;
                    }
    
                    .footer-wrapper {
                        padding: 30px 0;
                        max-width: 1120px;
                        margin: 0 auto; 
                        height: 100%; 
                    }


                    .footer-inner-content-wrapper {
                        display: flex;
                        flex-direction: column;
                        margin: 0;
                        align-items: center;
                    }
                    
                    .footer-left {
                        width: 100%;
                        display: flex;
                        flex-direction: column;
                        flex-wrap: wrap;
                        margin-top: 20px;
                    }
                    .contact-follow {
                        width: 30%;
                    }
                    .logo {
                        width: 90%;
                        margin-top: -20px;
                    }
                    .info-wrapper {
                        width: 100%;
                        padding-right: 0px;
                        align-items: center;
                        margin-bottom: 23px;
                    }
                    .info-wrapper h3 {
                        font-size: 12px;
                    }
                    .footer-link-1 h2, .footer-link-2 h2, .contact-us h2, .follow-us h2 {
                        font-size: 16px;
                        margin-bottom: 8px;
                    }
                
                    .footer-link-1 a, .footer-link-2 a {
                        font-size: 12px;
                        margin-bottom: 4px;
                    }

                    .hyperlink-column {
                        width: 100%;
                    }

                    .footer-link-1, .contact-us {
                        width: 45%;
                    }

                    .footer-link-2, .follow-us {
                        width: 55%;
                    }

                    .contact-follow {
                        width: 100%;
                        margin-left: 0px;
                        margin-top: 0px;
                        display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                    }

                    .contact-us p {
                        font-size: 10px;
                    }

                    .form-reach-us {
                      margin-bottom: 8px;
                    }

                    .social-media-wrapper {
                        display: flex;
                        justify-content: center;
                    }


                    .copyright span {
                        font-size: 10px;
                        line-height: 12px;
                        margin-right: 4px;
                    }

                    .copyright p {
                        margin: 0;
                        font-family: Open Sans;
                        font-size: 10px;
                        font-style: normal;
                        font-weight: 600;
                        line-height: 12px;
                        letter-spacing: 0em;
                        text-align: left;
                    }

                    .footer.bottom .logo {
                      width: 253px !important;
                    }

                    .footer.bottom .footer-inner-content-wrapper .hyperlink-column {
                      display: flex;
                      flex-direction: column;
                      justify-content: center;
                      text-align: center;
                    }

                    .footer.bottom .page {
                      display: none;
                    }

                    .reach-us {
                      width: 90%;
                      min-width: 90%;
                    }

                    .footer.bottom {
                      height: 234px;
                    }

                    .footer.upper h2 {
                      font-family: Bahnschrift;
                      font-style: normal;
                      font-weight: 600;
                      font-size: 12px;
                      line-height: 14px;
                      text-align: center;
                    }

                    .footer.upper p {
                      font-family: Calibri;
                      font-style: italic;
                      font-weight: normal;
                      font-size: 9px;
                      line-height: 11px;
                      text-align: center;
                    }

                    .form-reach-us input, .form-reach-us textarea {
                      font-family: Calibri;
                      width: 100%;    
                      border: 1px solid #0E0E0E;
                      background: transparent;
                      padding: 6px 10px;
                      font-size: 12px;
                      line-height: 15px;
                      color: #757575;
                    }
    
                    .footer-link h2 {
                      font-family: Bahnschrift;
                      font-style: normal;
                      font-weight: 600;
                      font-size: 12px;
                      line-height: 14px;
                      text-align: center;
                      text-transform: uppercase;
                    }

                    .footer-shape {
                      display: none;
                    }

                    .form-subscribe input {
                      font-family: Calibri;
                      width: 100%;    
                      border: 1px solid #FFFFFF;
                      background: #FFFFFF;
                      padding: 6px 10px;
                      font-size: 10px;
                      line-height: 12px;
                      color: #757575;
                      border-radius: 5px 0 0 5px;
                    }
    
                    .form-subscribe {
                      display: flex;
                      margin-bottom: 12px;
                      width: 80%;
                    }
    
                    .form-subscribe .black-button-wrapper {
                      border-radius: 0 5px 5px 0 !important;
                    }

                    .form-subscribe .black-button-wrapper p {
                      font-style: normal;
                      font-weight: normal;
                      font-size: 10px;
                      line-height: 12px;
                    }

                    .footer-link {
                      align-items: center;
                    }

                    .footer-description {
                      font-family: Calibri;
                      font-style: italic;
                      font-weight: normal;
                      font-size: 9px;
                      line-height: 11px;
                      text-align: center;
                      margin-bottom: 8px;
                    }

                }
                `
      })]
    });
  }

}

/* harmony default export */ const footer = (footer_Header);
;// CONCATENATED MODULE: ./containers/layout.js





function Layout({
  children,
  title
}) {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    children: [/*#__PURE__*/jsx_runtime_.jsx(head, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)("main", {
      children: [/*#__PURE__*/jsx_runtime_.jsx(header, {}), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "margin-container"
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "main-content",
        children: [children, " "]
      }), /*#__PURE__*/jsx_runtime_.jsx(footer, {})]
    }), /*#__PURE__*/jsx_runtime_.jsx("style", {
      children: `
          .container {
            width: 100vw;
            padding: 0;
          }

          .margin-container {
            margin-top: 76px;
          }
          
          .container-inner {
            max-width: 1160px;
            margin: 0 auto;
            padding: 12px 20px;
          }

          .container.banner {
            background-color: #F5FFEE;
          }
          
          @media only screen and (max-width: 800px) {
            .margin-container {
              margin-top: 56px;
            }
          }
        `
    })]
  });
}

/***/ }),

/***/ 431:
/***/ (() => {

/* (ignored) */

/***/ })

};
;