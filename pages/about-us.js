import Layout from "../containers/layout";
import Section from "../components/section";
import Slider from "react-slick";

export default function Home() {
  
	const settings = {
		dots: true,
		arrows: true,
		infinite: true,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 1,
    variableWidth: true
	};

  return (
      <Layout
        title="test">     
        <div className="container">
          <div className="header-about">
            <img className="green-tetra-img" src="/static/images/green-tetra.svg" />
            <div className="header-about-wrapper">
              <div className="header-img">
                <img src="http://cdn.eramitra.com/images_article/original/DSC00749.jpg" />
              </div>
              <h3 className="section-title">About Us</h3>
              <p className="section-description">PT.	Era	Mitra	Perdana	provides	Scientific,	Laboratory	
              Equipment,	and	Services. We	are	highly	experienced	and	a	trusted	partner	for	
              Government	Institutions,	Universities,	and	Industries	to	
              supply,	install,	and	provide	after	sales	services	in	the	area	
              of	scientific	and	laboratory	equipment	since	1999.</p>
            </div>
            <style>
              {`
                .header-about-wrapper {
                  display: flex;
                  flex-direction: column;
                  align-items: center;
                  margin-top: -250px;
                  padding-bottom: 78px;
                }

                .header-img {
                  width: 566px;
                  height: 345px;
                  border-radius: 20px;
                  margin-bottom: 70px;
                }

                .header-img img {
                  object-fit: cover;
                  width: 566px;
                  height: 345px;
                  border-radius: 20px;
                }

                .header-about .section-title, .header-about .section-description {
                  text-align: center;
                  width: 55%;
                }

                @media only screen and (max-width: 1024px){       
                  .header-about-wrapper {
                    margin-top: -150px;
                  }
                }

                @media only screen and (max-width: 800px){
                  .header-img {
                    width: 183px;
                    height: 111px;
                    margin-bottom: 35px;
                  }

                  .header-about .section-title, .header-about .section-description {
                    text-align: center;
                    width: 70%;
                  }
                  
                  .header-about-wrapper {
                    margin-top: -50px;
                  }

                  .header-img img {
                    width: 183px;
                    height: auto;
                    border-radius: 10px;
                  }
                }
              `}
            </style>
          </div>
          <Section 
            element_id="vision-about" 
            background="#FBFBFB" 
            icon={[
              {
                image: "shape-6.svg",
                vertical: "top",
                horizontal: "left"
              },
              {
                image: "shape-7.svg",
                vertical: "bottom",
                horizontal: "right"
              }
            ]} 
            height="618px" >
            <div className="container-inner">
              <div className="vision-about-wrapper">
                <div className="about-us-description">
                  <h3 className="section-title">Vision</h3>
                  <p className="section-description">PT.	Era	Mitra	Perdana	provides	Scientific,	Laboratory	
                  Equipment,	and	Services. We	are	highly	experienced	and	a	trusted	partner	for	
                  Government	Institutions,	Universities,	and	Industries	to	
                  supply,	install,	and	provide	after	sales	services	in	the	area	
                  of	scientific	and	laboratory	equipment	since	1999.</p>
                </div>
                <img src="/static/images/vision.svg" />
              </div>
            </div>
            <style>
              {`
                .vision-about-wrapper {
                  height: 100%;
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                  padding: 0 10px;
                }

                .vision-about-wrapper img {
                  height: 60%;
                  width: auto;
                  margin-left: 75px;
                }

                .vision-about .icon-shape-0 {
                  height: 40% !important;
                  width: auto !important;
                }
                
                .vision-about .icon-shape-1 {
                  display: none;
                }

                @media only screen and (max-width: 1024px){
                  .vision-about .icon-shape-0 {
                    height: 25% !important;
                    width: auto !important;
                  }
                }

                @media only screen and (max-width: 800px){

                  .vision-about-wrapper {
                    flex-direction: column-reverse;
                    justify-content: center;
                  }

                  .vision-about-wrapper img {
                    height: 126px;
                    width: auto;
                    margin-left: 0;
                    margin-bottom: 15px;
                  }

                  .vision-about {
                    height: 308px !important;
                  }

                  .vision-about .icon-shape-0 {
                    height: 50% !important;
                    width: auto !important;
                  }

                  
                }
              `}
            </style>
          </Section>

          <Section 
            element_id="mission-about" 
            background="#FFFFFF" 
            icon={[
              {
                image: "shape-5.svg",
                vertical: "bottom",
                horizontal: "right"
              }
            ]} 
            height="618px" >
            <div className="container-inner">
              <div className="mission-about-wrapper">
                <img src="/static/images/mission.svg" />
                <div className="mission-description">
                  <h3 className="section-title">Mission</h3>
                  <p className="section-description">PT.	Era	Mitra	Perdana	provides	Scientific,	Laboratory	
                  Equipment,	and	Services. We	are	highly	experienced	and	a	trusted	partner	for	
                  Government	Institutions,	Universities,	and	Industries	to	
                  supply,	install,	and	provide	after	sales	services	in	the	area	
                  of	scientific	and	laboratory	equipment	since	1999.</p>
                </div>
              </div>
            </div>
            <style>
              {`
                .mission-about-wrapper {
                  height: 100%;
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                  padding: 0 10px;
                  z-index: 1;
                }

                .mission-about-wrapper img {
                  height: 70%;
                  width: auto;
                  margin-right: 75px;
                }

                @media only screen and (max-width: 1024px){
                  .mission-about .icon-shape-0 {
                    height: 40%;
                    width: auto;
                  }
                }
                @media only screen and (max-width: 800px){

                  .mission-about-wrapper {
                    flex-direction: column;
                    justify-content: center;
                  }

                  .mission-about-wrapper img {
                    height: 126px;
                    width: auto;
                    margin-right: 0;
                    margin-bottom: 15px;
                  }

                  .mission-about {
                    height: 308px !important;
                  }

                  .mission-about .icon-shape-0, .mission-about .icon-shape-1 {
                    display: none;
                  }
                }
              `}
            </style>
          </Section>
          
          <Section 
            element_id="values" 
            background="#FBFBFB" 
            // icon={[
            //   {
            //     image: "shape-4.svg",
            //     vertical: "top",
            //     horizontal: "left"
            //   }
            // ]} 
            height="618px" >
            <div className="container-inner">

              <div className="values-wrapper">
              <h3 className="section-title">Values</h3>
                <div className="values-card-wrapper">
                  <div className="values-card">
                  <img src="/static/images/bond.svg" />
                    <h3 className="section-title">Bond</h3>
                      <p className="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer augue risus</p>
                  </div>
                  <div className="values-card">
                  <img src="/static/images/performance.svg" />
                    <h3 className="section-title">Performance</h3>
                      <p className="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer augue risus</p>
                  </div>
                  <div className="values-card">
                  <img src="/static/images/process.svg" />
                    <h3 className="section-title">Process</h3>
                      <p className="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer augue risus</p>
                  </div>
                </div>
              </div>
            </div>
            <style>
              {`
                .values-wrapper {
                  height: 100%;
                  padding: 0 10px;
                  display: flex;
                  flex-direction: column;
                  align-items: center;
                  justify-content: center;
                }

                .values-wrapper .section-title {
                  text-align: center;
                }
                
                .values-card-wrapper {
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                }

                .values-card {
                  display: flex;
                  flex-direction: column;
                  justify-content: center;
                  align-items: center;
                  padding: 0 15px;
                }

                .values-card .section-title {
                  font-style: normal;
                  font-weight: 600;
                  font-size: 24px;
                  line-height: 29px;
                  text-align: center;
                  margin-bottom: 8px;
                }

                .values-card .section-description {
                  font-weight: normal;
                  font-size: 20px;
                  line-height: 24px;
                  text-align: center;
                  max-width: 280px;
                }

                .values-card img {
                  height: 150px;
                }

                @media only screen and (max-width: 800px){
                  .values-card-wrapper {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    justify-content: space-between;
                  }

                  .values-wrapper .section-title {
                    margin-bottom: 20px;
                  }

                  .values {
                    height: 538px !important;
                  }

                  .values-card img {
                    height: 66px;
                  }

                  .values-card {
                    margin-bottom: 10px;
                  }

                  .values-card .section-title {
                    font-family: Bahnschrift;
                    font-style: normal;
                    font-weight: 600;
                    font-size: 16px;
                    line-height: 19px;
                    text-align: center;
                    margin-bottom: 8px;
                  }
  
                  .values-card .section-description {
                    font-family: Calibri;
                    font-style: italic;
                    font-weight: normal;
                    font-size: 12px;
                    line-height: 15px;
                    text-align: center;
                    color: #666A66;
                    max-width: 215px;
                  }
m  

                }
              `}
            </style>
          </Section>
          
          <Section 
            element_id="what-we-do-best" 
            background="#FFFFFF" 
            icon={[
              {
                image: "shape-2.svg",
                vertical: "top",
                horizontal: "right"
              }
            ]} 
            height="1000px" >
            <div className="container-inner">
              <div className="what-we-do-best-wrapper">
                  <h3 className="section-title">What We Do Best</h3>
                  <p className="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer augue risus, tempus ac leo vel, laoreet congue quam. Sed convallis gravida maximus. </p>
                      
                  <div className="main-content">
                    <div className="card-wrapper">
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-1.svg" />
                        <p>High Quality with <br></br> Competitive Price</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-2.svg" />
                        <p>Installation &amp; <br></br>Training</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-3.svg" />
                        <p>Accessories &amp;<br></br> Sparepart</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-4.svg" />
                        <p>Maintenance &amp; <br></br> Contract Service</p>
                      </div>
                    </div>
                    <img className="what-we-do-best-img" src="/static/images/what-we-do-best.svg" />
                  </div>
              </div>
            </div>
            <style>
              {`
                .what-we-do-best-wrapper {
                  height: 100%;
                  display: flex;
                  flex-direction: column;
                  justify-content: center;
                  align-items: center;
                }
                .what-we-do-best-wrapper .section-title {
                  text-align: center;
                }
                
                .what-we-do-best-wrapper .section-description {
                  text-align: center;
                  max-width: 741px;
                }

                .what-we-do-best-wrapper .main-content {
                  display: flex;
                  justify-content: space-between;
                  width: 100%;
                  margin-top: 76px;
                }

                .what-we-do-best-wrapper .card-wrapper {
                  display: flex;
                  flex-direction: column;
                  justify-content: space-between;
                  padding-bottom: 20px;
                }

                .wwdb-card {
                  background: #F8F8F8;
                  border-radius: 20px;
                  padding: 17px 19px;
                  display: flex;
                  align-items: center;
                  max-width: 439px;
                  margin-bottom: 10px;
                }

                .wwdb-card p {
                  font-family: Bahnschrift;
                  font-style: normal;
                  font-weight: normal;
                  font-size: 24px;
                  line-height: 29px;
                  color: #000000;
                  margin: 0 0 0 25px;
                  padding-right: 50px;
                }

                .what-we-do-best-img {
                  width: 60%;
                }


                @media only screen and (max-width: 1024px){
                  .what-we-do-best .icon-shape-0 {
                    height: 25% !important;
                    width: auto !important;
                  }
                }

                @media only screen and (max-width: 800px){
                  .what-we-do-best {
                    height: 452px !important;
                  }

                  .wwdb-card {
                    background: #F8F8F8;
                    border-radius: 10px;
                    padding: 8px;
                    display: flex;
                    align-items: center;
                    max-width: 217px !important;
                  }
  
                  .wwdb-card img {
                    height: 45px;
                    width: auto;
                  }

                  .wwdb-card p {
                    font-family: Bahnschrift;
                    font-style: normal;
                    font-weight: normal;
                    font-size: 12px;
                    line-height: 14px;
                    margin: 0 0 0 12px !important;
                    padding-right: 50px;
                  }

                  .what-we-do-best-img {
                    display: none;
                  }

                  .what-we-do-best-wrapper {
                    padding: 25px 0;
                  }

                  .what-we-do-best-wrapper .main-content {
                    display: flex;
                    justify-content: center;
                    width: 100%;
                    margin-top: 26px;
                    height: 100%;
                  }

                  .what-we-do-best-wrapper .card-wrapper {
                    padding-bottom: 0px;
                  }

                  .what-we-do-best .icon-shape-0 {
                    height: 120px !important;
                    bottom: 0;
                    top: auto;
                  }
                }
              `}
            </style>
          </Section>
        </div>
        <style>
          {`
            .container {
              width: 100vw;
              padding: 0;
            }
  
            .container-inner {
              max-width: 1120px;
              margin: 0 auto;
              height: 100%;
            }
  
            .container.banner {
              background-color: #F5FFEE;
            }

            .section-title {
              font-family: Bahnschrift;
              font-style: normal;
              font-weight: 600;
              font-size: 48px;
              color: #000000;
              margin: 0 0 24px 0;
            }

            .section-description {
              font-family: Calibri;
              font-style: italic;
              font-weight: normal;
              font-size: 20px;
              line-height: 24px;
              color: #666A66;
              margin: 0;
            }

            .green-tetra-img {
              width: 100%;
            }

               
            @media only screen and (max-width: 800px){
              .section-title {
                font-family: Bahnschrift;
                font-style: normal;
                font-weight: 600;
                font-size: 18px;
                line-height: 22px;
                text-align: center;
                margin-bottom: 10px;
              }

              .section-description {
                font-family: Calibri;
                font-style: italic;
                font-weight: normal;
                font-size: 12px;
                line-height: 15px;
                text-align: center;
                color: #666A66;
                padding: 0 15px;
              }
            }
          `}
        </style>
      </Layout>
  );
}
