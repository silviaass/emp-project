import Layout from "../containers/layout";
import CareerCard from "../components/card/CareerCard";
import Section from "../components/section";

export default function Home() {
  
	const settings = {
		dots: true,
		arrows: true,
		infinite: true,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 1,
    variableWidth: true
	};

  return (
      <Layout
        title="test">     
        <div className="container">
        <Section 
            element_id="career" 
            background="#1BA58A"  
            height="351px"
            icon={[
              {
                image: "career.svg",
                vertical: "bottom",
                horizontal: "left"
              },
              {
                image: "shape-3.svg",
                vertical: "top",
                horizontal: "right"
              }
            ]}  >
            <div className="container-inner">
              <div className="career-wrapper">
                <div className="career-description">
                  <h3 className="section-title">Up for a challenge? 
                    Let’s join us!</h3>
                </div>
              </div>
            </div>
            <style>
              {`
                .career-wrapper {
                  height: 100%;
                  display: flex;
                  align-items: center;
                  justify-content: center;
                }

                
                .career-wrapper .career-description {
                  display: flex;
                  align-items: center;
                }

                .career-wrapper .section-title {
                  margin: 0;
                  max-width: 496px;
                  color: #FFFFFF !important;
                  font-weight: normal !important;
                  text-align: center;
                }

                .career-wrapper .btn-wrapper a {
                  padding: 21px 50px;
                  background: #FFFFFF;
                  font-family: Bahnschrift;
                  font-style: normal;
                  font-weight: normal;
                  font-size: 24px;
                  line-height: 29px;
                  color: #1BA58A;
                  border-radius: 20px;
                }
                
              `}
            </style>
          </Section>
          
          <Section 
            element_id="what-we-do-best" 
            background="#FFFFFF" 
            icon={[
              {
                image: "shape-2.svg",
                vertical: "top",
                horizontal: "right"
              }
            ]} 
            height="1000px" >
            <div className="container-inner">
              <div className="what-we-do-best-wrapper">
                  <h3 className="section-title">What We Do Best</h3>
                  <p className="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer augue risus, tempus ac leo vel, laoreet congue quam. Sed convallis gravida maximus. </p>
                      
                  <div className="main-content">
                    <div className="card-wrapper">
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-1.svg" />
                        <p>High Quality with <br></br> Competitive Price</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-2.svg" />
                        <p>Installation &amp; <br></br>Training</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-3.svg" />
                        <p>Accessories &amp;<br></br> Sparepart</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-4.svg" />
                        <p>Maintenance &amp; <br></br> Contract Service</p>
                      </div>
                    </div>
                    <img src="/static/images/what-we-do-best.svg" />
                  </div>
              </div>
            </div>
            <style>
              {`
                .what-we-do-best-wrapper {
                  height: 100%;
                  display: flex;
                  flex-direction: column;
                  justify-content: center;
                  align-items: center;
                }
                .what-we-do-best-wrapper .section-title {
                  text-align: center;
                }
                
                .what-we-do-best-wrapper .section-description {
                  text-align: center;
                  max-width: 741px;
                }

                .what-we-do-best-wrapper .main-content {
                  display: flex;
                  justify-content: space-between;
                  width: 100%;
                  margin-top: 76px;
                }

                .what-we-do-best-wrapper .card-wrapper {
                  display: flex;
                  flex-direction: column;
                  justify-content: space-between;
                  padding-bottom: 20px;
                }

                .wwdb-card {
                  background: #F8F8F8;
                  border-radius: 20px;
                  padding: 17px 19px;
                  display: flex;
                  align-items: center;
                  max-width: 439px;
                }

                .wwdb-card p {
                  font-family: Bahnschrift;
                  font-style: normal;
                  font-weight: normal;
                  font-size: 24px;
                  line-height: 29px;
                  color: #000000;
                  margin: 0 0 0 25px;
                  padding-right: 50px;
                }
              `}
            </style>
          </Section>
        </div>
        <style>
          {`
            .container {
              width: 100vw;
              padding: 0;
            }
  
            .container-inner {
              max-width: 1120px;
              margin: 0 auto;
              height: 100%;
            }
  
            .container.banner {
              background-color: #F5FFEE;
            }

            .section-title {
              font-family: Bahnschrift;
              font-style: normal;
              font-weight: 600;
              font-size: 48px;
              color: #000000;
              margin: 0 0 24px 0;
            }

            .section-description {
              font-family: Calibri;
              font-style: italic;
              font-weight: normal;
              font-size: 20px;
              line-height: 24px;
              color: #666A66;
              margin: 0;
            }
          `}
        </style>
      </Layout>
  );
}
