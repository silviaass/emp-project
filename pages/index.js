import Layout from "../containers/layout";
import Section from "../components/section";
import Slider from "react-slick";

export default function Home() {
  
	const settings = {
		dots: false,
		arrows: true,
		infinite: false,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1450,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          dots: false
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: false,
          dots: false
        }
      }
    ]
	};

  const settings2 = {
		dots: true,
		arrows: false,
		infinite: true,
    autoplay: true,
    speed: 1000,
    autoplaySpeed: 5000,
		slidesToShow: 1,
		slidesToScroll: 1
	};

  return (
      <Layout
        title="test">     
        <div className="container">
          <div className="intro-slider">
            <Slider {...settings2} className="banner-slider">
                      
                      <div>
                        <img
                          src="https://cdn.eramitra.com/slider/8/nabertherm-images-bub.png"
                        />
                      </div>
                      <div>
                        <img
                          src="https://cdn.eramitra.com/slider/7/linse-images-kat.jpg"
                        />
                      </div>
                      <div>
                        <img
                          src="https://cdn.eramitra.com/slider/5/picarro-images-vhe.png"
                        />
                      </div>
                    </Slider>
            <style>
              {`
                .banner-slider img {
                  width: 100%;
                }
              `}
            </style>
          </div>
          <Section 
            element_id="about-us" 
            background="#FBFBFB" 
            icon={[
              {
                image: "shape-1.svg",
                vertical: "bottom",
                horizontal: "right"
              },
              {
                image: "shape-6.svg",
                vertical: "top",
                horizontal: "left"
              }
            ]} 
            height="618px" >
            <div className="container-inner">
              <div className="about-us-wrapper">
                <img src="http://cdn.eramitra.com/images_article/original/DSC00749.jpg" />
                <div className="about-us-description">
                  <h3 className="section-title">About Us</h3>
                  <p className="section-description">PT.	Era	Mitra	Perdana	provides	Scientific,	Laboratory	
                  Equipment,	and	Services. We	are	highly	experienced	and	a	trusted	partner	for	
                  Government	Institutions,	Universities,	and	Industries	to	
                  supply,	install,	and	provide	after	sales	services	in	the	area	
                  of	scientific	and	laboratory	equipment	since	1999.</p>
                </div>
              </div>
            </div>
            <style>
              {`
                .about-us-wrapper {
                  height: 100%;
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                }

                .about-us-wrapper img {
                  height: 345px;
                  width: auto;
                  border-radius: 20px;
                  margin-right: 30px;
                }

                .about-us .icon-shape-1 {
                  display: none;
                }


                @media only screen and (max-width: 800px){

                  .about-us-wrapper {
                    flex-direction: column;
                    justify-content: center;
                  }

                  .about-us-wrapper img {
                    height: 111px;
                    width: auto;
                    border-radius: 20px;
                    margin-right: 0;
                    margin-bottom: 17px;
                  }

                  .about-us {
                    height: 308px !important;
                  }

                  .about-us .icon-shape-1 {
                    display: block;
                  }

                  .about-us .icon-shape-0 {
                    display: none;
                  }
                }
              `}
            </style>
          </Section>

          
          <Section 
            element_id="what-we-do-best" 
            background="#FFFFFF" 
            icon={[
              {
                image: "shape-2.svg",
                vertical: "top",
                horizontal: "right"
              }
            ]} 
            height="1000px" >
            <div className="container-inner">
              <div className="what-we-do-best-wrapper">
                  <h3 className="section-title">What We Do Best</h3>
                  <p className="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer augue risus, tempus ac leo vel, laoreet congue quam. Sed convallis gravida maximus. </p>
                      
                  <div className="main-content">
                    <div className="card-wrapper">
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-1.svg" />
                        <p>High Quality with <br></br> Competitive Price</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-2.svg" />
                        <p>Installation &amp; <br></br>Training</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-3.svg" />
                        <p>Accessories &amp;<br></br> Sparepart</p>
                      </div>
                      <div className="wwdb-card">
                        <img src="/static/images/wwdb-4.svg" />
                        <p>Maintenance &amp; <br></br> Contract Service</p>
                      </div>
                    </div>
                    <img className="what-we-do-best-img" src="/static/images/what-we-do-best.svg" />
                  </div>
              </div>
            </div>
            <style>
              {`
                .what-we-do-best-wrapper {
                  height: 100%;
                  display: flex;
                  flex-direction: column;
                  justify-content: center;
                  align-items: center;
                }
                .what-we-do-best-wrapper .section-title {
                  text-align: center;
                }
                
                .what-we-do-best-wrapper .section-description {
                  text-align: center;
                  max-width: 741px;
                }

                .what-we-do-best-wrapper .main-content {
                  display: flex;
                  justify-content: space-between;
                  width: 100%;
                  margin-top: 76px;
                }

                .what-we-do-best-wrapper .card-wrapper {
                  display: flex;
                  flex-direction: column;
                  justify-content: space-between;
                  padding-bottom: 20px;
                }

                .wwdb-card {
                  background: #F8F8F8;
                  border-radius: 20px;
                  padding: 17px 19px;
                  display: flex;
                  align-items: center;
                  max-width: 439px;
                  margin-bottom: 10px;
                }

                .wwdb-card p {
                  font-family: Bahnschrift;
                  font-style: normal;
                  font-weight: normal;
                  font-size: 24px;
                  line-height: 29px;
                  color: #000000;
                  margin: 0 0 0 25px;
                  padding-right: 50px;
                }

                .what-we-do-best-img {
                  width: 60% !important;
                }

                @media only screen and (max-width: 800px){
                  .what-we-do-best {
                    height: 452px !important;
                  }

                  .wwdb-card {
                    background: #F8F8F8;
                    border-radius: 10px;
                    padding: 8px;
                    display: flex;
                    align-items: center;
                    max-width: 217px !important;
                  }
  
                  .wwdb-card img {
                    height: 45px;
                    width: auto;
                  }

                  .wwdb-card p {
                    font-family: Bahnschrift;
                    font-style: normal;
                    font-weight: normal;
                    font-size: 12px;
                    line-height: 14px;
                    margin: 0 0 0 12px !important;
                    padding-right: 50px;
                  }

                  .what-we-do-best-img {
                    display: none;
                  }

                  .what-we-do-best-wrapper {
                    padding: 25px 0;
                  }

                  .what-we-do-best-wrapper .main-content {
                    display: flex;
                    justify-content: center;
                    width: 100%;
                    margin-top: 26px;
                    height: 100%;
                  }

                  .what-we-do-best-wrapper .card-wrapper {
                    padding-bottom: 0px;
                  }

                  .what-we-do-best .icon-shape-0 {
                    height: 120px !important;
                    bottom: 0;
                    top: auto;
                  }
                }
              `}
            </style>
          </Section>

          <Section 
            element_id="brands" 
            background="#FBFBFB"  
            height="263px" >
            <div className="container-inner">
              <div className="brands-wrapper">
                <div className="slider-brands-wrapper">
                  <div className="slider-wrapper">
                    <Slider {...settings} className="brand-slider">
                      
                      <div>
                        <img
                          src="https://cdn.eramitra.com/kategori/1/micromeritics-images-nqk.JPG"
                        />
                      </div>
                      <div>
                        <img
                          src="https://cdn.eramitra.com/kategori/1/micromeritics-images-nqk.JPG"
                        />
                      </div>
                      <div>
                        <img
                          src="https://cdn.eramitra.com/kategori/1/micromeritics-images-nqk.JPG"
                        />
                      </div>

                      <div>
                        <img
                          src="https://cdn.eramitra.com/kategori/1/micromeritics-images-nqk.JPG"
                        />
                      </div>

                      <div>
                        <img
                          src="https://cdn.eramitra.com/kategori/1/micromeritics-images-nqk.JPG"
                        />
                      </div>
                    </Slider>
                  </div>
                </div>
                <div className="brands-description">
                  <h3 className="section-title">Brands</h3>
                  <p className="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
              </div>
            </div>
            <style>
              {`
                .brands-wrapper {
                  height: 100%;
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                }

                .brands-wrapper .section-title {
                  margin: 0;
                }
                
                .brands-wrapper .section-description {
                  max-width: 280px;
                }

                .brands-slider {
                  width: 100%;
                  margin-right: 54px;
                }

                .brands-item {
                  height: 90px;
                  width: auto;
                }

                .slider-wrapper {
                  width: 100%;
                  display: block;
                  margin-left: 20px;
                }

                .slider-brands-wrapper {
                  display: inline-block;
                }
                .brand-slider {
                  display: flex;
                  align-items: center;
                  justify-content: center;
                }
                .brand-slider div {
                  width: 50vw;
                  // display: flex;
                  // justify-content: center;
                  // align-items: center;
                }
                .brand-slider div img {
                  width: 200px;
                  padding: 0 10px;
                }

              
                @media only screen and (max-width: 1300px){
                  .brand-slider div {
                    width: 60vw;
                  }
                }
              @media only screen and (max-width: 800px){
                  .brands-wrapper {
                    flex-direction: column-reverse;
                    justify-content: center;
                  }

                  .brands-description {
                    margin-bottom: 8px;
                  }

                  .brands {
                    height: 215px !important;
                  }

                  .brand-slider div img {
                    width: 100px;
                    padding: 0 10px;
                  }
                }

              `}
            </style>
          </Section>
          <Section 
            element_id="discover" 
            background="#FFFFFF"  
            height="768px"
            icon={[
              {
                image: "shape-4.svg",
                vertical: "top",
                horizontal: "left"
              }
            ]}  >
            <div className="container-inner">
              <div className="discover-wrapper">
                <div className="discover-description">
                <h3 className="section-title">Discover</h3>
                  <p className="section-description">Discover our latest news and info. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer augue risus, tempus ac leo vel, laoreet congue quam. Sed convallis gravida maximus.</p>
             
                </div>
              </div>
            </div>
            <style>
              {`
                .discover-wrapper {
                  height: 100%;
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                }

                .discover-wrapper .section-description {
                  max-width: 350px;
                }
                

                @media only screen and (max-width: 800px){
                  .discover {
                    height: 436px !important;
                  }

                  .discover .icon-shape-0 {
                    width: 78px !important;
                  }
                }
              `}
            </style>
          </Section>
          <Section 
            element_id="career" 
            background="#1BA58A"  
            height="351px"
            icon={[
              {
                image: "career.svg",
                vertical: "bottom",
                horizontal: "left"
              },
              {
                image: "shape-3.svg",
                vertical: "top",
                horizontal: "right"
              }
            ]}  >
            <div className="container-inner">
              <div className="career-wrapper">
                <div className="career-description">
                  <h3 className="section-title">Up for a challenge? 
                    Let’s join us!</h3>
                </div>
                <div className="btn-wrapper">
                  <a href="http://localhost:3000/career">
                    More Info
                  </a>
                </div>
              </div>
            </div>
            <style>
              {`
                .career-wrapper {
                  height: 100%;
                  display: flex;
                  align-items: center;
                  justify-content: center;
                }

                
                .career-wrapper .career-description {
                  display: flex;
                  align-items: center;
                }

                .career-wrapper .section-title {
                  margin: 0;
                  max-width: 496px;
                  color: #FFFFFF !important;
                  font-weight: normal !important;
                }

                .career-wrapper .btn-wrapper a {
                  padding: 21px 50px;
                  background: #FFFFFF;
                  font-family: Bahnschrift;
                  font-style: normal;
                  font-weight: normal;
                  font-size: 24px;
                  line-height: 29px;
                  color: #1BA58A;
                  border-radius: 20px;
                }
                

                @media only screen and (max-width: 970px){
                  .career-wrapper .section-title {
                    max-width: 300px !important;
                  }

                  .career-wrapper {
                    justify-content: flex-end;
                  }
                
                }

                @media only screen and (max-width: 800px){
                  .career {
                    height: 137px !important;
                  }

                  .career .icon-shape-0 {
                    height: 108px !important;
                  }

                  .career .icon-shape-1 {
                    display: none;
                  }

                  .career-wrapper {
                    height: 100%;
                    display: flex;
                    align-items: center;
                    justify-content: flex-start;
                    margin-left: 100px;
                  }
                  
                  .career-wrapper .section-title {
                    max-width: 140px !important;
                    text-align: left; 
                    font-family: Bahnschrift;
                    font-style: normal;
                    font-weight: normal;
                    font-size: 14px;
                    line-height: 17px;
                  }

                  .career-wrapper .btn-wrapper a {
                    font-style: normal;
                    font-weight: normal;
                    font-size: 12px;
                    line-height: 14px;
                    border-radius: 10px;
                    padding: 10px 22px;
                  }
                }
              `}
            </style>
          </Section>
        </div>
        <style>
          {`
            .container {
              width: 100vw;
              padding: 0;
            }
  
            .container-inner {
              max-width: 1120px;
              margin: 0 auto;
              height: 100%;
              padding: 0 20px;
            }
  
            .container.banner {
              background-color: #F5FFEE;
            }

            .section-title {
              font-family: Bahnschrift;
              font-style: normal;
              font-weight: 600;
              font-size: 48px;
              color: #000000;
              margin: 0 0 24px 0;
            }

            .section-description {
              font-family: Calibri;
              font-style: italic;
              font-weight: normal;
              font-size: 20px;
              line-height: 24px;
              color: #666A66;
              margin: 0;
            }

            
            @media only screen and (max-width: 800px){
              .section-title {
                font-family: Bahnschrift;
                font-style: normal;
                font-weight: 600;
                font-size: 18px;
                line-height: 22px;
                text-align: center;
                margin-bottom: 10px;
              }

              .section-description {
                font-family: Calibri;
                font-style: italic;
                font-weight: normal;
                font-size: 12px;
                line-height: 15px;
                text-align: center;
                color: #666A66;
              }
            }
          `}
        </style>
      </Layout>
  );
}
