import React from 'react'

class CareerCard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        isToggleOn: false
    };
    this.handleClick = this.handleClick.bind(this);
  }



  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

  render() {
    const props = this.props

    return (
            <div className="career-card">
              <div className="career-card-content">
                <h5 className="career-title">{props.title}</h5>
                { this.state.isToggleOn ? <span className="material-icons" onClick={this.handleClick}>remove</span> : 
                <span className="material-icons" onClick={this.handleClick}>add</span> }
              </div>
                { this.state.isToggleOn &&
                <div className="career-expand">
                    {props.description}
                </div> }
                    
            <style>
                    {`
                        .career-card {
                          display: flex;
                          flex-direction: column;
                          margin-bottom: 16px;
                        }

                        .career-card-content {
                            display: flex;
                            align-items: center;
                            justify-content: space-between;
                            padding: 10px 35px;
                            border-radius: 8px;
                            background: #F0F8FB;
                        }

                        .career-card-content .career-title {
                            font-family: Open Sans;
                            font-size: 20px;
                            font-style: normal;
                            font-weight: 600;
                            line-height: 27px;
                            letter-spacing: 0em;
                            text-align: left;
                            margin: 0;
                            padding-right: 8px;
                        }

                        .career-card .career-expand {
                            font-family: Open Sans;
                            font-size: 17px;
                            font-style: normal;
                            font-weight: 600;
                            line-height: 23px;
                            letter-spacing: 0em;
                            text-align: left;
                            padding: 6px 35px;                            
                        }

                        .career-card .career-expand a {
                          text-decoration: underline;
                          color: #5294BD;
                        }

                        @media only screen and (max-width: 800px){ 
                          .career-card {
                            margin-bottom: 8px;
                          }

                          .career-card-content {
                            padding: 8px 16px;
                          }

                          .career-card-content .career-title {
                            font-family: Open Sans;
                            font-size: 14px;
                            font-style: normal;
                            font-weight: 600;
                            line-height: 16px;
                            letter-spacing: 0em;
                            text-align: left;
                          }

                          .career-card-content .material-icons {
                            font-size: 12px;
                            line-height: 18px;
                          }

                          .career-card .career-expand {
                            font-family: Open Sans;
                            font-size: 12px;
                            font-style: normal;
                            font-weight: 600;
                            line-height: 14px;
                            letter-spacing: 0em;
                            text-align: left;
                            padding: 6px 16px;                            
                          }
                        }
                    `}
            </style>
            </div>
      )
    }
}

export default CareerCard