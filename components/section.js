export default function Section( { children, element_id, background, icon, height }) {
  return (
    <div className={element_id} >
        {children}
        {
          icon && icon.length > 0 && icon.map((shape, id) => (
            <>
            <img key={id} className={`icon-shape-${id}  ${shape.vertical ? shape.vertical : ''} ${shape.horizontal ? shape.horizontal : ''}`} src={`/static/images/${shape.image}`} />
            <style>
            {`
              .${element_id} .icon-shape-${id} {
                position: absolute;
                z-index: 0;
              }

              .${element_id} .icon-shape-${id}.top {
                top: 0;
              }

              .${element_id} .icon-shape-${id}.bottom {
                bottom: 0;
              }

              .${element_id} .icon-shape-${id}.right {
                right: 0;
              }

              .${element_id} .icon-shape-${id}.left {
                left: 0;
              }
          `}
            </style>
            </>)
          )
        }
      <style>
        {`
          .${element_id} {
            width: 100%;
            background: ${background};
            position: relative;
            height: ${height};
          }

        `}
      </style>
    </div>
  );
}

