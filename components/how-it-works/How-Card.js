import React from 'react'

class HowCard extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const props = this.props

    return (
        <div className="how-it-works-card">
            <img className="how-it-works-picture" src={props.picture} />
            <p>{props.text}</p>
            <style>
                {`
                    .how-it-works-card {
                        display: flex;
                        flex-direction: column;
                        align-content: center;
                        padding: 20px;
                    }

                    .how-it-works-card p {
                        font-family: Open Sans;
                        font-size: 16px;
                        font-style: normal;
                        font-weight: 500;
                        line-height: 20px;
                        text-align: center;
                        max-width: 200px;
                    }

                    .how-it-works-card .how-it-works-picture {
                        height: 110px;
                        width: auto;
                    }

                `}
            </style>
        </div>
      )
    }
}

export default HowCard