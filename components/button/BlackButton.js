import React from "react";

class BlackButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const props = this.props;

    return props.link ? (
      <a className={`btn-black ${props.element_id}`} href={props.link}>
        <div className="black-button-wrapper">
          <p>{props.text}</p>
        </div>
        <style>
          {`
                    .black-button-wrapper {
                        background: #626262;
                        display: flex;
                        flex-wrap: wrap;
                        justify-content: center;
                        align-content: center;
                        width: fit-content;
                        padding: 4px 18px;
                        border-radius: 10px;
                    }

                    .black-button-wrapper p {
                        color: white;
                        font-family: Bahnschrift;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 18px;
                        line-height: 22px;
                        margin: 0;
                    }

                    @media only screen and (max-width: 800px){
                        .black-button-wrapper p {
                          font-family: Bahnschrift;
                          font-style: normal !important;
                          font-weight: normal;
                          font-size: 10px;
                          line-height: 12px;
                        }

                        .black-button-wrapper {
                          padding: 5px 10px;
                          border-radius: 5px;
                        }

                        .btn-black {
                          display: flex;
                          justify-content: center;
                        }
                    }
                `}
        </style>
      </a>
    ) : props.onClick && (   
      <a className={`btn-black ${props.element_id}`} onClick={props.onClick}>
        <div className="black-button-wrapper">
          <p>{props.text}</p>
        </div>
        <style>
          {`
                    .black-button-wrapper {
                        background: #626262;
                        display: flex;
                        flex-wrap: wrap;
                        justify-content: center;
                        align-content: center;
                        width: fit-content;
                        padding: 8px 22px;
                        border-radius: 10px;
                    }

                    .black-button-wrapper p {
                        color: white;
                        font-family: Bahnschrift;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 18px;
                        line-height: 22px;
                        margin: 0;
                    }

                    @media only screen and (max-width: 800px){
                        .black-button-wrapper p {
                            font-family: Bahnschrift;
                            font-style: normal !important;
                            font-weight: normal;
                            font-size: 10px;
                            line-height: 12px;
                        }

                        .black-button-wrapper {
                          padding: 5px 10px;
                          border-radius: 5px;
                        }

                        .btn-black {
                          display: flex;
                          justify-content: center;
                        }
                    }
                `}
        </style>
      </a>)
  }
}

export default BlackButton;
