import React from "react";
import Link from "next/link";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState((state) => ({
      isToggleOn: !state.isToggleOn,
    }));
  }

  render() {
    const props = this.props;

    return (
      <div className="navbar-header">
        {this.state.isToggleOn && (
          <div className="sidebar mobile-only">
            <div className="logo-wrapper">

            <a href="http://localhost:3000/">
                <img className="logo" src="/static/images/logo.png" />
              </a>
              {/* <span className="material-icons" onClick={this.handleClick}>
                close
              </span> */}
            </div>
            <div className="navbar-link">
            <Link href="http://localhost:3000/about-us">
                  <a>
                    About Us
                  </a>
                </Link>
                <Link href="http://localhost:3000">
                  <a>
                    Products
                  </a>
                </Link>
                <Link href="http://localhost:3000">
                  <a>
                    News &amp; Info
                  </a>
                </Link>
                <Link href="http://localhost:3000">
                  <a>
                    Career
                  </a>
                </Link>                
                <Link href="http://localhost:3000">
                  <a>
                    Contact Us
                  </a>
                </Link>
            </div>
          </div>
        )}

        {this.state.isToggleOn && (
          <div
            className="sidebar-background mobile-only"
            onClick={this.handleClick}
          ></div>
        )}

        <div className="navbar-content-wrapper">
          <div className="navbar-inner-content-wrapper">
            <div className="logo-wrapper">
              <span
                className="material-icons mobile-only"
                onClick={this.handleClick}
              >
                menu
              </span>
              <a href="http://localhost:3000/">
                <img className="logo" src="/static/images/logo.png" />
              </a>
            </div>
            <div className="navbar-link desktop-only">
              <div className="navbar-link-wrapper">
                <Link href="http://localhost:3000/about-us">
                  <a>
                    About Us
                  </a>
                </Link>
                <Link href="http://localhost:3000">
                  <a>
                    Products
                  </a>
                </Link>
                <Link href="http://localhost:3000">
                  <a>
                    News &amp; Info
                  </a>
                </Link>
                <Link href="http://localhost:3000">
                  <a>
                    Career
                  </a>
                </Link>                
                <Link href="http://localhost:3000">
                  <a>
                    Contact Us
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <style>
          {`
                .desktop-only {
                    display: block;
                }

                .mobile-only {
                    display: none;
                }
                
                .navbar-header {
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 3;
                    width: 100vw;
                    padding: 0;
                }

                .navbar-content-wrapper {
                    width: 100%;
                    position: relative;
                    box-shadow: 0px 4px 10px rgba(145, 158, 171, 0.1);
                    background-color: #FFF;
                    padding: 0 20px;
                }

                .navbar-inner-content-wrapper {
                    display: flex;
                    align-items: center;
                    justify-content: space-between;
                    flex-wrap: wrap;
                    max-width: 1120px;
                    height: 76px;
                    margin: 0 auto;
                }

                .navbar-inner-content-wrapper .logo-wrapper {
                    display: flex;
                    align-items: center;
                }

                .navbar-inner-content-wrapper .logo {
                    width: 90%;
                }

                .navbar-link-wrapper {
                  display: flex;
                  align-items: center;
                }

                .navbar-link a {
                    font-family: 'Bahnschrift';
                    font-weight: 400;
                    font-size: 14px;
                    margin-left: 55px;
                }

                .navbar-link a.active {
                    font-family: 'Bahnschrift';
                    font-weight: 500;
                    margin-left: 55px;
                    text-decoration: none;
                    color: #4B8343;
                }

                @media only screen and (max-width: 800px){
                    .mobile-only {
                        display: block;
                    }
                    
                    .desktop-only {
                        display: none;
                    }

                    .navbar-inner-content-wrapper {
                        height: 56px;
                    }
                    
                    .navbar-inner-content-wrapper .logo, .sidebar .logo {
                        width: 150px;
                        margin: 0 16px;
                        padding-top: 2px;
                    }

                    .navbar-header {
                        position: absolute;
                    }

                    .sidebar {
                        width: 80%;
                        height: 100vh;
                        background-color: white;
                        position: fixed;
                        z-index: 100;
                        top: 0;
                        left: 0;
                    }

                    .sidebar-background {
                        width: 100%;
                        height: 100%;
                        z-index: 99;
                        background-color: rgba(0, 0, 0, 0.1);
                        position: fixed;
                        top: 0;
                        left: 0;
                    }

                    .sidebar .logo-wrapper {
                        display: flex;
                        align-items: center;
                        height: 56px;
                        justify-content: space-between;
                        padding: 16px 32px 16px 14px;
                        // border-bottom: 1px solid rgb(235,235,235,1);
                    }

                    .sidebar .navbar-link {
                        display: flex;
                        flex-direction: column;
                        margin-top: 45px;
                    }

                    .sidebar .navbar-link a {
                      font-family: Bahnschrift;
                      font-style: normal;
                      font-weight: normal;
                      font-size: 18px;
                      line-height: 22px;
                      text-transform: uppercase;
                        text-align: left;
                        margin: 16px 0 0 32px;
                    }

                }
                `}
        </style>
      </div>
    );
  }
}

export default Header;
