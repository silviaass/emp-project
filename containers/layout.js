import WebHead from "./head";
import Header from "./header";
import Footer from "./footer";

export default function Layout( { children, title }) {
  return (
    <div>
      <WebHead />

      <main>
        <Header />
        <div className="margin-container"></div>
        <div className="main-content">{children} </div>
        <Footer />
      </main>

      <style>
        {`
          .container {
            width: 100vw;
            padding: 0;
          }

          .margin-container {
            margin-top: 76px;
          }
          
          .container-inner {
            max-width: 1160px;
            margin: 0 auto;
            padding: 12px 20px;
          }

          .container.banner {
            background-color: #F5FFEE;
          }
          
          @media only screen and (max-width: 800px) {
            .margin-container {
              margin-top: 56px;
            }
          }
        `}
      </style>
    </div>
  );
}

